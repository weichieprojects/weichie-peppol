<?php

namespace WeichieCom\Peppol\Facades;

use Illuminate\Support\Facades\Facade;
use WeichieCom\Peppol\Services\IxorDocsClient;

class Peppol extends Facade
{
    protected static function getFacadeAccessor()
    {
        return IxorDocsClient::class; // The name used to bind the instance in the service container
    }
}
