<?php

namespace App\Enums;

enum PeppolMessageStatus: string
{
    case ACKNOWLEDGED = 'AB';
    case IN_PROCESS = 'IP';
    case UNDER_QUERY = 'UQ';
    case CONDITIONALLY_ACCEPTED = 'CA';
    case REJECTED = 'RE';
    case ACCEPTED = 'AP';
    case PAID = 'PD';

    /**
     * Get the description of the enum value
     */
    public function description(): string
    {
        return match ($this) {
            self::ACKNOWLEDGED => 'Acknowledged',
            self::IN_PROCESS => 'In process',
            self::UNDER_QUERY => 'Under query',
            self::CONDITIONALLY_ACCEPTED => 'Conditionally accepted',
            self::REJECTED => 'Rejected (Final status)',
            self::ACCEPTED => 'Accepted (approved)',
            self::PAID => 'Paid (Final status, The status code “Paid” will probably not be used in relation to credit notes)',
        };
    }
}
