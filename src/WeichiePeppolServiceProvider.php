<?php

namespace WeichieCom\Peppol;

use Illuminate\Support\ServiceProvider;
use WeichieCom\Peppol\Services\IxorDocsClient;

class WeichiePeppolServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/peppol.php', 'peppol');

        $this->app->singleton(IxorDocsClient::class, function ($app) {
            return new IxorDocsClient(
                config('peppol.client_api'),
                config('peppol.client_secret')
            );
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/peppol.php' => config_path('peppol.php'),
        ], 'config');
    }
}
