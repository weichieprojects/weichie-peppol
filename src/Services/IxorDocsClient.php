<?php

namespace WeichieCom\Peppol\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class IxorDocsClient
{
    private $clientApi;

    private $clientSecret;

    private $clientEnvironment;

    private $accessToken;

    // Fixed URLs for authentication and API base
    private $authUrl = 'https://ixordocs.apiable.io/api/oauth2/oauth-token';

    private $apiStagingBaseUrl = 'https://api.ixordocs-stg.com/v1/peppol';

    private $apiEnterpriseBaseUrl = 'https://api.ixordocs.com/v1/peppol';

    // Define common API endpoints relative to the API base URL
    private $endpoints = [
        'invoices' => '/invoices',
        'send_invoice' => '/invoices/send',
        'scan_document' => '/scan',
        'status' => '/status',
    ];

    /**
     * IxorDocsClient constructor
     *
     * @param  string  $clientApi
     * @param  string  $clientSecret
     */
    public function __construct($clientApi, $clientSecret, $clientEnvironment = 'staging')
    {
        $this->clientApi = $clientApi;
        $this->clientSecret = $clientSecret;
        $this->clientEnvironment = $clientEnvironment;
        $this->accessToken = $this->getAccessToken();
    }

    /**
     * Get the base URL based on the environment
     *
     * @return string
     */
    private function getBaseUrl()
    {
        return $this->clientEnvironment === 'enterprise' ? $this->apiEnterpriseBaseUrl : $this->apiStagingBaseUrl;
    }

    /**
     * Retrieve the OAuth2 access token
     *
     * @return string
     */
    private function getAccessToken()
    {
        return Cache::remember('ixordocs.access_token', 300, function () { // Cache for 1 hour
            return $this->fetchAccessToken();
        });
    }

    /**
     * Fetch the OAuth2 access token from the dedicated authentication server
     *
     * @return string|null
     */
    public function fetchAccessToken()
    {
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
        ])->post($this->authUrl, [
            'clientId' => $this->clientApi,
            'clientSecret' => $this->clientSecret,
        ]);

        if ($response->successful()) {
            return $response->json()['access_token'] ?? null;
        }

        return null;
    }

    /**
     * Register a participant using the OAuth2 access token
     *
     * @return array|null
     */
    public function registerParticipant(array $participantData)
    {
        $accessToken = $this->getAccessToken();
        if (! $accessToken) {
            // Handle missing token error
            return null;
        }

        $response = Http::withToken($accessToken)
            ->withHeaders(['Content-Type' => 'application/json'])
            ->post($this->getBaseUrl().'/participant-registrations', $participantData);

        if ($response->successful()) {
            return $response->json();
        }

        // Log error details for troubleshooting
        \Log::error('Failed to register participant', [
            'status' => $response->status(),
            'body' => $response->body(),
        ]);

        throw new \Exception('Failed to register participant: ' . $response->status() . ' - ' . $response->body());
    }

    public function deregisterParticipant($participantIdentifier, $participentScheme = 'iso6523-actorid-upis')
    {
        $accessToken = $this->getAccessToken();
        if (! $accessToken) {
            // Handle missing token error
            return null;
        }

        $data = [
            'scheme' => $participentScheme,
            'value' => $participantIdentifier,
        ];

        $response = Http::withToken($accessToken)
            ->withHeaders(['Content-Type' => 'application/json'])
            ->delete($this->getBaseUrl().'/participant-registrations', $data);

        if ($response->successful()) {
            return $response->json();
        }

        // Log error details for troubleshooting
        \Log::error('Failed to delete participant', [
            'status' => $response->status(),
            'body' => $response->body(),
        ]);

        return null; // Handle error as required
    }

    public function lookupParticipant($participantIdentifier, $participentScheme = 'iso6523-actorid-upis')
    {
        $accessToken = $this->getAccessToken();
        if (! $accessToken) {
            // Handle missing token error
            return null;
        }

        $data = [
            'scheme' => $participentScheme,
            'value' => $participantIdentifier,
        ];

        $response = Http::withToken($accessToken)
            ->withHeaders(['Content-Type' => 'application/json'])
            ->get($this->getBaseUrl().'/participant-registrations/lookup', $data);

        if ($response->successful()) {
            return $response->json();
        }

        // Log error details for troubleshooting
        \Log::error('Failed to register participant', [
            'status' => $response->status(),
            'body' => $response->body(),
        ]);

        return null; // Handle error as required
    }

    /**
     * Check the Peppol status
     *
     * @return array|null
     */
    public function getStatus()
    {
        $response = Http::withToken($this->accessToken)
            ->get($this->getBaseUrl().$this->endpoints['status']);

        if ($response->successful()) {
            return 'Peppol is up and running!';
        } else {
            return 'Peppol is down!';
        }
    }
}
