<?php

return [
    'client_api' => env('PEPPOL_CLIENT_API', ''),
    'client_secret' => env('PEPPOL_CLIENT_SECRET', ''),
    'client_environment' => env('PEPPOL_CLIENT_ENVIRONMENT', 'sandbox'),
];
